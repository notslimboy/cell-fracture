﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructable : MonoBehaviour
{
    [SerializeField] private GameObject shaterredcubes;
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Bola")
        {
            Instantiate(shaterredcubes, transform.position, transform.rotation);
            Destroy(gameObject);
        }
        
    }
}
