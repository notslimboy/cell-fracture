﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet: MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float lifeSpan = 2f;
    private float ballTimer;
    // Start is called before the first frame update
    void Start()
    {
        ballTimer = lifeSpan;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
        ballTimer -= Time.deltaTime;
        if (ballTimer <= 0f)
        {
            Destroy(gameObject);
        }
    }
}
