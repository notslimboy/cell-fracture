﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLooking : MonoBehaviour
{
    public float mouseSensitivity = 100f;
    public Transform player;
    float rotationX = 0f;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float xAxis = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float yAxis = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
        rotationX -= yAxis;
        rotationX = Mathf.Clamp(rotationX, -90f, 90f);
        player.Rotate(Vector3.up * xAxis);
        transform.localRotation = Quaternion.Euler(rotationX, 0f, 0f);
    }
}
