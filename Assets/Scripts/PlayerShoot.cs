﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public GameObject bola;
    public Camera player;
    // Start is called before the first frame update
    void Start()
    {   
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject bolaPrefab = Instantiate(bola);
            bolaPrefab.transform.position = player.transform.position + player.transform.forward;
            bolaPrefab.transform.forward = player.transform.forward;
        }
    }
}
