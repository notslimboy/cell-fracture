﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RandomColor : MonoBehaviour
{
    public MeshRenderer[] renderer;

    private void Start()
    {
        Color newColor = Random.ColorHSV();
        ApplyMaterial(newColor,0);
    }

    private void ApplyMaterial(Color color, int targetedMaterialIndex)
    {
        Material generatedMaterial = new Material(Shader.Find("Standard"));
        generatedMaterial.SetColor("_Color", color);
        for (int i = 0; i < renderer.Length; i++)
        {
            renderer[i].material = generatedMaterial;
        }
    }
}
