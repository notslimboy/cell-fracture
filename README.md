# Cell Fracture

A simulation that uses 3D objects as the main test which will then give a split effect when subjected to a projectile or other object

![Cell Fracture](https://gitlab.com/notslimboy/cell-fracture/uploads/beabf4c1410a6310f4184ebf9cfa48c0/image.png)